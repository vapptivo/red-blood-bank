import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class IndexService {

  uri = 'http://localhost:3000/bloodbank';

  constructor(private http: HttpClient) { }

  register(contact) {
    this.http.post(`${this.uri}/addcontact`, contact)
        .subscribe(res => console.log('Contact Added Successfully'));
  }

  /*authenticate(credential) {
    this.http.post(`${this.uri}/authenticate`, credential)
        .subscribe(res => this.callbackfn(res));
  }

  callbackfn(res) {
      console.log("res::", res);
  }*/

  raiseRequest(transaction) {
    this.http.post(`${this.uri}/raiserequest`, transaction)
        .subscribe(res => console.log('twilio processing'));
  }
}
