import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { MapsAPILoader, MouseEvent } from '@agm/core';
import { IndexService } from '../services/index.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  messageSuccess: boolean = false;
  
  lat: number = 19.708293802130548;
  lng: number = 78.95652264975695;
  zoom: number = 3.5;

  // for db
  latLoc : number;
  lngLoc : number;

  requestForm: FormGroup;
  constructor(private fb: FormBuilder, private is: IndexService, private elRef:ElementRef) {
    this.requestBloodForm();
  }

  public handleAddressChange(address) {
    console.log(address.geometry.location.lng());
    console.log(address.geometry.location.lat());
    console.log(address.geometry.location.toJSON());
    console.log(address.geometry.viewport.getNorthEast());
    
    this.lng = address.geometry.location.lng();
    this.lat  = address.geometry.location.lat();
    this.zoom = 15;

    this.markers[0] = {
      lat: this.lat,
      lng: this.lng,
      draggable: true
    };

    this.latLoc = this.lat;
    this.lngLoc = this.lng;
  }

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }
  
  mapClicked($event: MouseEvent) {
    this.markers[0] = {
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    };

    this.latLoc = this.markers[0].lat;
    this.lngLoc = this.markers[0].lng;

    console.log("latitude:", this.latLoc, ":longitude:", this.lngLoc);
  }
  
  markerDragEnd(m: markerIntr, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }
  
  markers: markerIntr[] = [
	  
  ]

  
  requestBloodForm() {
    this.requestForm = this.fb.group({
      blood_group: ['', Validators.required ],
      donor_count: ['', Validators.required ],
      within_hours: ['', Validators.required ]
    })
  }

  // convenience getter for easy access to form fields
  get reqf() { return this.requestForm.controls; }

  requestRaise(formData) {
    var value = formData.value;
    console.log("requested data:", value);
    console.log("latitude:", this.latLoc, ":longitude:", this.lngLoc);

    var mapElement = this.elRef.nativeElement.querySelector('.gmapsearchbox');
    console.log("map_address:", mapElement.value);

    var transaction = {
      "blood_group": value.blood_group,
      "donor_count": value.donor_count,
      "within_hours": value.within_hours,
      "latitude": this.latLoc,
      "longitude": this.lngLoc
    }


    
    this.is.raiseRequest(transaction);

    this.messageSuccess = true;
    setTimeout(()=>{    //<<<---    using ()=> syntax
        this.messageSuccess = false;
    }, 3000);
  }
  

  ngOnInit() {
  }

}

// just an interface for type safety.
interface markerIntr {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
}
