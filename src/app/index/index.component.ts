import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { IndexService } from '../services/index.service';

import { MustMatch } from '../helpers/must-match.validator';

import { MapsAPILoader, MouseEvent } from '@agm/core';
//import { } from '@types/googlemaps';


import { GooglePlaceDirective } from 'ngx-google-places-autocomplete/ngx-google-places-autocomplete.directive';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  
@ViewChild('places') places: GooglePlaceDirective;
    @ViewChild('search' ) public searchElement: ElementRef;

    messageSuccess:boolean = false;

    lat: number = 19.708293802130548;
    lng: number = 78.95652264975695;
    zoom: number = 3.5;

    // for db
    latLoc : number;
    lngLoc : number;

  @ViewChild('bloodBankMap') gmapElement: any;
  map: google.maps.Map;

  loginForm: FormGroup;
  registrationForm: FormGroup;
  constructor(private fb: FormBuilder, private is: IndexService, private elRef:ElementRef, private mapsAPILoader: MapsAPILoader, private ngZone: NgZone) {
    this.registerForm();
    this.signinForm();
  }

  public handleAddressChange(address) {
    console.log(address.geometry.location.lng());
    console.log(address.geometry.location.lat());
    console.log(address.geometry.location.toJSON());
    console.log(address.geometry.viewport.getNorthEast());
    
    this.lng = address.geometry.location.lng();
    this.lat  = address.geometry.location.lat();
    this.zoom = 15;

    this.markers[0] = {
      lat: this.lat,
      lng: this.lng,
      draggable: true
    };

    this.latLoc = this.lat;
    this.lngLoc = this.lng;
  }

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }
  
  mapClicked($event: MouseEvent) {
    this.markers[0] = {
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    };

    this.latLoc = this.markers[0].lat;
    this.lngLoc = this.markers[0].lng;

    console.log("latitude:", this.latLoc, ":longitude:", this.lngLoc);
  }
  
  markerDragEnd(m: markerIntr, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }
  
  markers: markerIntr[] = [
	  
  ]

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirm_password.value;

    return pass === confirmPass ? null : { notSame: true }     
  }

  registerForm() {
    this.registrationForm = this.fb.group({
      first_name: ['', Validators.required ],
      last_name: ['', Validators.required ],
      password: ['', Validators.required ],
      confirm_password: ['', Validators.required ],
      email: ['', Validators.required ],
      phone_number: ['', Validators.required ],
      /*address: ['', Validators.required],*/
      blood_group: ['', Validators.required],
      gender: ['male']
    },
    {
      validator: MustMatch('password', 'confirm_password')
    });
  }

  // convenience getter for easy access to form fields
  get regf() { return this.registrationForm.controls; }

  register(formData) {
    var value = formData.value;
    console.log("first_name", value.first_name);
    console.log("last_name", value.last_name);
    /*console.log("password", value.password);
    console.log("confirm_password", value.confirm_password);*/
    console.log("email", value.email);
    console.log("phone_number", value.phone_number);
    /*console.log("address", value.address);*/
    console.log("blood_group", value.blood_group);
    console.log("gender", value.gender);
    console.log("latitude:", this.latLoc, ":longi:", this.lngLoc);


    var contact = {
      "first_name": value.first_name,
      "last_name": value.last_name,
      "password": value.confirm_password,
      "email": value.email,
      "phone_number": value.phone_number,
      "blood_group": value.blood_group,
      "gender": value.gender,
      "latitude": this.latLoc,
      "longitude": this.lngLoc
    }

    this.is.register(contact);

    this.messageSuccess = true;
    setTimeout(()=>{    //<<<---    using ()=> syntax
        this.messageSuccess = false;
    }, 3000);
  }

  
  signinForm() {
    this.loginForm = this.fb.group({
      email: ['', Validators.required ],
      password: ['', Validators.required ]
    })
  }

  // convenience getter for easy access to form fields
  get lnf() { return this.loginForm.controls; }

  doLogin(formData) {
    console.log("login form data value:",formData.value);
    var value = formData.value
    /*if(value.email == "admin") {*/
      // do routing to next page
      window.location.href = "home";
    //}
  }

  ngOnInit() {
       
  }

}

// just an interface for type safety.
interface markerIntr {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
}